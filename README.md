# Mount Olympus

My personal mount olympus. Overseeing the empire.

## Installation instructions

### Install the package

Run the following command to install the package into your Laravel app:
```
composer require hermes\admin ^1.0
```

### Publish the package's assets

Run the following command to publish the admin panel package assets:
```
php artisan publish:vendor --provider="Hermes\Admin\Providers\AdminServiceProvider"
```

This will publish:
- Config file ```app/config/admin.php```
- Views (layout, pages & partials)
- Breadcrumbs directory in ```app/routes```
- Admin.scss file in ```app/resources/sass```
- Vue component files in ```app/resources/js/components```
- Migrations in ```app/database/migrations```
- Seeders in ```app/database/seeders```

### Configure the breadcrumbs package

For the breadcrumbs of the admin panel pages we'll be using the [laravel-breadcrumbs package by Dave James Miller](https://github.com/davejamesmiller/laravel-breadcrumbs) because it's awesome and contains all our requirements for supporting the modular admin panel setup we're going for.

Publish the package's config file with the following command:
```
php artisan vendor:publish --provider="DaveJamesMiller\Breadcrumbs\BreadcrumbsServiceProvider"
```

Edit the ```config/breadcrumbs.php``` file you just published and update it to support loading the individual admin panel breadcrumb files and the breadcrumbs for the frontend of the application. It should look like this:
```
'files' => array_merge(
    glob(base_path('routes/breadcrumbs/*.php')),
    glob(base_path('routes/breadcrumbs/admin/*.php'))
),
```







