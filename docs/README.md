# Hermes Admin

## Features

- Full featured UI out of the box
- Extandable and customizable
- Plugin system for packages to add their own functionality
- Fully integrated with the Hermes\Auth package
- And more...

## Backend dependencies

- [Hermes\Settings](https://bitbucket.org/mtolympus/settings) ```^1.0```
- [Cviebrock\eloquent-sluggable](https://github.com/cviebrock/eloquent-sluggable) ```^4.6```
- [Laracasts\flash](https://github.com/laracasts/flash) ```^3.0```
- [Prettus\l5-repository](https://github.com/andersao/l5-repository) ```^2.6```

## Frontend dependencies

- [Vue](https://vuejs.org)
- [Vuetify](https://vuetifyjs.org)
- [Fontawesome](https://fontawesome.com)