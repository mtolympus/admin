<!-- _coverpage.md -->

![logo](_media/logo-black.png)

# Hermes Admin Package<small>v1.0.0</small>

> Advanced administration panel in 1 minute

- Fully hookable UI
- Navigation manager
- Easy integration for packages

*Coming soon:*
- Automatic CRUD generation

[Get Started](#get-started)