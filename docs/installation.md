# Installation instructions

The developer friendly administration panel package for your Laravel 5.7+ applications.

## Require the package

Run the following command to install the package and it's dependencies:
```
composer require hermes\admin ^1.0
```

## Publish the package's assets

Run the following command to publish the package's assets:
```
php artisan publish:vendor --provider="Hermes\Admin\Providers\AdminServiceProvider"
```

## Install frontend dependencies

### Admin stylesheet

Add the published `admin.scss` file to the `webpack.mix.js` file so it will be compiled:

```js
mix.sass('resources/sass/admin.scss', 'public/css');
```

### Vue components

Make sure the Vue components that have been published are loaded. If you're using the new recursive loading of components snippet in your `app.js` file; you shouldn't have to do anything. If you're not make sure you manually load the components.

## Setting up the database

### Register the seeder

After publishing a new Seeder will be available. Run the following command so the seeder can be found by the application:

```
composer dumpautoload
```

### Run the migrations

Run the following command to migrate the new migrations and refresh & seed the database afterwards:

```
php artisan migrate && php artisan migrate:refresh --seed
```
