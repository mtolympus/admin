# Usage instructions

The admin package was intended to be easily extendable by packages. So the developer can load in different packages and have a fully featured admin panel available instantly.

## Dashboard manager

**TODO** A simple manager to handle adding cards to the dashboard with useful information.

## Navigation manager

This class, `NavigationManager`, is available as a Facade and can be used to programatically manage the admin panel's navigation.
The package also has a built in UI for managing the navigation which can be found under `Settings` > `Navigation settings`.

## Registering a package

### Service provider

You register your package with the admin package in your package's service provider. Within your `boot()` method you can add the following call to the `AdminManager` facade to register your package:

```php
use AdminManager;
...
public function boot()
{
    // Wait until the app has fully booted; otherwise the Facades we'll be using won't be available yet
    $this->app->booted(function() {
        
        // Register the package with the admin panel
        AdminManager::registerPackage("packageName", function($navigation, $permissions, $settings) {

            // Use the NavigationManager service to register your navigation headers, links & sublinks
            $navigation->registerHeader();
            $navigation->registerHeaders();

            // Use the PermissionManager service to register your package's permissions
            $permissions->registerPermissionGroups();
            $permissions->registerPermissionGroup();
            $permissions->registerPermissions();
            $permissions->registerPermission();

            // Use the SettingsManager service to register your package's settings
            $settings->createSettingGroups();
            $settings->createSettings();
            $settings->createSettingsForGroup();

        });

    });
}
```

### Views

Each view should:

- Extend the admin template (the default one or your own)
- Include breadcrumb rendering
- Include the feedback partial

A page could look something like:

```php
@extends("admin::layouts.master")

@section("content")

    <div class="wrapper">

        <!-- Breadcrumbs -->
        {!! Breadcrumbs::render("admin.") !!}

        <!-- Header -->
        <div class="page-header">
            <div class="page-header__text">
                <div class="page-header__title"></div>
                <div class="page-header__subtitle"></div>
            </div>
        </div>
        
        <!-- Feedback -->
        @include("admin::partials.feedback")

        <!-- Content -->
        <div class="content-card elevation-1">
            Content here
        </div>

    </div>

@stop
```

### Breadcrumbs

Your package's breadcrumbs should be defined in a `.php` file named after your package and should be published to the `routes/breadcrumbs/admin` directory where it will be automatically loaded.