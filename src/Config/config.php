<?php

/**
 * Hermes Admin Package Configuration
 * 
 * @version     1.0.0
 * @author      Nick Verheijen <verheijen.webdevelopmen@gmail.com>
 */

return [
    
    /**
     * Page title
     * This will be the base text of the <title> tag on all admin panel pages
     * 
     * @var     string
     */
    "page_title" => "Admin panel",

    


];