<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminNavigationLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_navigation_links', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('admin_navigation_header_id')->nullable();
            $table->integer('admin_navigation_link_id')->nullable();
            $table->integer('order')->default(0);
            $table->string('name');
            $table->string('text');
            $table->string('href')->default('#');
            $table->string('icon')->nullable();
            $table->string('route')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_navigation_links');
    }
}
