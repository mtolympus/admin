<?php

use Hermes\Settings\Models\Setting;
use Hermes\Settings\Models\SettingType;
use Hermes\Settings\Models\SettingGroup;
use Hermes\Admin\Models\AdminNavigationLink;
use Hermes\Admin\Models\AdminNavigationHeader;

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    public function run()
    {
        $this->emptyDatabase();
        $this->seedAdminNavigation();
        $this->seedAdminSettings();
    }

    private function emptyDatabase()
    {

        DB::table("admin_navigation_links")->delete();
        DB::table("admin_navigation_headers")->delete();
        DB::table("admin_processed_packages")->delete();
    }

    private function seedAdminNavigation()
    {
        // General
        $general = AdminNavigationHeader::create([
            "order" => 0,
            "name" => "general",
            "text" => "General",
        ]);
        $dashboard = AdminNavigationLink::create([
            "admin_navigation_header_id" => $general->id,
            "order" => 0,
            "name" => "dashboard",
            "text" => "Dashboard",
            "icon" => '<i class="fas fa-tachometer-alt"></i>',
            "href" => route('admin.dashboard'),
            "route" => "admin.dashboard"
        ]);

        // Settings
        $settings = AdminNavigationHeader::create([
            "order" => 1,
            "name" => "settings",
            "text" => "Settings"
        ]);
        $dashboard_settings = AdminNavigationLink::create([
            "admin_navigation_header_id" => $settings->id,
            "order" => 0,
            "name" => "dashboard_settings",
            "text" => "Dashboard settings",
            "icon" => '<i class="fas fa-cog"></i>',
            "href" => route("admin.settings.dashboard"),
            "route" => "admin.settings.dashboard",
        ]);
        $template_settings = AdminNavigationLink::create([
            "admin_navigation_header_id" => $settings->id,
            "order" => 1,
            "name" => "template_settings",
            "text" => "Template settings",
            "icon" => '<i class="fas fa-cog"></i>',
            "href" => route("admin.settings.template"),
            "route" => "admin.settings.template"
        ]);
        $navigation_settings = AdminNavigationLink::create([
            "admin_navigation_header_id" => $settings->id,
            "order" => 2,
            "name" => "navigation_settings",
            "text" => "Navigation settings",
            "icon" => '<i class="fas fa-cog"></i>',
            "href" => route("admin.settings.navigation"),
            "route" => "admin.settings.navigation",
        ]);
        $authentication_settings = AdminNavigationLink::create([
            "admin_navigation_header_id" => $settings->id,
            "order" => 3,
            "name" => "auth_settings",
            "text" => "Authentication settings",
            "icon" => '<i class="fas fa-cog"></i>',
            "href" => route("admin.settings.auth"),
            "route" => "admin.settings.auth"
        ]);
    }

    private function seedAdminSettings()
    {
        // Grab the types
        $text = SettingType::where("name", "text")->first();
        $number = SettingType::where("name", "number")->first();
        $boolean = SettingType::where("name", "boolean")->first();
        $select = SettingType::where("name", "select")->first();
        $image = SettingType::where("name", "image")->first();
        $file = SettingType::where("name", "file")->first();

        // Group
        $template = SettingGroup::create([
            "name" => "admin_template_settings",
            "label" => "Template settings",
            "description" => "Configure the admin panel template.",
        ]);

        // Settings
        $logo_image = Setting::create([
            "setting_group_id" => $template->id,
            "setting_type_id" => $image->id,
            "name" => "admin_template_logo_image",
            "label" => "Logo image",
            "description" => "Configure the logo image to display in the top left corner of the template.",
            "value" => "storage/images/admin/logo.png",
        ]);
        $logo_text = Setting::create([
            "setting_group_id" => $template->id,
            "setting_type_id" => $text->id,
            "name" => "admin_template_logo_text",
            "label" => "Logo text",
            "description" => "What should the text next to the logo say?",
            "value" => "Olympus"
        ]);
        $footer_text = Setting::create([
            "setting_group_id" => $template->id,
            "setting_type_id" => $text->id,
            "name" => "admin_template_footer_text",
            "label" => "Copyright text",
            "description" => "What copyright text should be displayed at the bottom of the admin template?",
            "value" => "Copyrighted by &copy; Verheijen Webdevelopment, 2019 - 2020."
        ]);
    }
}