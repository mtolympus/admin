<?php

namespace Hermes\Admin\Facades;

use Illuminate\Support\Facades\Facade;

class AdminManager extends Facade
{
    protected static function getFacadeAccessor()
    {
        return "adminManager";
    }
}