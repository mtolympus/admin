<?php

namespace Hermes\Admin\Facades;

use Exception;

use Hermes\Admin\Models\AdminNavigationLink;
use Hermes\Admin\Models\AdminNavigationHeader;

use Illuminate\Support\Facades\Facade;

class NavigationManager extends Facade
{
    protected static function getFacadeAccessor()
    {
        return "navigationManager";
    }

    /**
     * Register multiple headers
     * 
     * @param       array                           Array filled with data for the headers
     * @return      void
     */
    public function registerHeaders(array $headers)
    {
        if (count($headers) > 0)
        {
            foreach ($headers as $header)
            {
                $this->registerHeader($header);
            }
        }
    }

    /**
     * Register header
     * 
     * @param       array                           Array with data representing a header
     * @return      void
     */
    public function registerHeader(array $data)
    {
        // The fields that are required to create a navigation header
        $requiredFields = ["name", "text"];

        // Determine if the data we received contains the required fields
        foreach ($requiredFields as $requiredField)
        {
            if (!array_key_exists($requiredField, $data))
            {
                throw new Exception("Header is missing required field: ".$requiredField);
            }
        }

        // Attempt to find an existing header with the desired name
        $header = AdminNavigationHeader::where("name", $data["name"])->first();

        // If the header does not exist yet
        if (!$header)
        {
            // Compose the header
            $headerData = [
                "name" => $data["name"],
                "text" => $data["text"],
            ];

            // If a specific order has been given
            if (array_key_exists("order", $data))
            {
                // Add the order to the header data
                $headerData["order"] = $data["order"];

                // Move down all headers starting from the desired order (so it'll still make sense)
                $this->moveDownHeadersFrom($data["order"]);
            }
            // If no order has been given
            else
            {
                // Append it to the bottom
                $headerData["order"] = AdminNavigationHeader::all()->count() + 1;
            }

            // Create the header
            $header = AdminNavigationHeader::create($headerData);
        }

        // If we received links to process as well
        if (array_key_exists("links", $data) and count($data["links"]) > 0)
        {
            $this->registerLinks($data["name"], $data["links"]);
        }
    }

    /**
     * Register links
     * 
     * @param       string                          Name of the header we want to add links to
     * @param       array                           Array with data representing the links
     * @return      void
     */
    public function registerLinks(string $headerName, array $data)
    {
        if (count($data) > 0)
        {
            foreach ($data as $linkData)
            {
                $this->registerLink($headerName, $linkData);
            }
        }
    }
    
    /**
     * Register a link
     * 
     * @param       string                          Name of the header we want to add the link to
     * @param       array                           Array of data representing the link
     * @return      void
     */
    public function registerLink(string $headerName, array $data)
    {
        // Find the header we want to add the link to
        $header = AdminNavigationHeader::where("name", $headerName)->first();

        // If we found the header
        if ($header)
        {
            // The required fields for creating a navigation link
            $requiredFields = ["name", "text", "href", "route"];

            // Determine if the link's data was valid
            foreach ($requiredFields as $requiredField)
            {
                if (!array_key_exists($requiredField, $data))
                {
                    throw new Exception("Navigation link is missing required field: ".$requiredField);
                }
            }

            // Attempt to find the link we want to create to prevent duplicates
            $link = AdminNavigationLink::where("admin_navigation_header_id", $header->id)->where("name", $data["name"])->first();

            // If the link does not exist yet
            if (!$link)
            {
                // Compose the link's data
                $linkData = [
                    "admin_navigation_header_id" => $header->id,
                    "name" => $data["name"],
                    "text" => $data["text"],
                    "href" => $data["href"],
                    "route" => $data["route"]
                ];

                // If we received a specific order
                if (array_key_exists("order", $data))
                {
                    $linkData["order"] = $data["order"];

                    $this->moveLinksDownForHeaderFrom($header, $linkData);
                }
                // If we did not receive a specific order
                else
                {
                    $linkData["order"] = $header->links->count() + 1;
                }
                
                // Add the optional icon if we received it
                if (array_key_exists("icon", $data)) $linkData["icon"] = $data["icon"];

                // Create the link
                $link = AdminNavigationLink::create($linkData);

                // If we received sublinks
                if (array_key_exists("sublinks", $data) and count($data["sublinks"]) > 0)
                {
                    // Register the fuckers
                    $this->registerSublink($headerName, $data["name"], $data["sublinks"]);
                }
            }
        }
    }

    /**
     * Register sublink
     * 
     * @param       string                          Name of the header the link, we want to add a sublink to, belongs to
     * @param       string                          Name of the link we want to add a sublink to
     * @param       array                           Array of data representing the sublink
     * @return      void
     */
    public function registerSublink(string $headerName, string $linkName, array $data)
    {
        // Find the header
        $header = AdminNavigationHeader::where("name", $headerName)->first();
        if ($header)
        {
            // Find the link
            $link = AdminNavigationLink::where("admin_navigation_header_id", $header->id)->where("name", $linkName)->first();
            if ($link)
            {
                // The fields that are required to register a sublink
                $requiredFields = ["name", "text", "href", "route"];

                // Validate the sublink data we received
                foreach ($requiredFields as $requiredField)
                {
                    if (!array_key_exists($requiredField, $data))
                    {
                        throw new Exception("Navigation sublink is missing required field: ".$requiredField);
                    }
                }

                // If the data is valid
                if ($valid)
                {
                    // Compose the sublink data
                    $sublinkData = [
                        "admin_navigation_link_id" => $link->id,
                        "name" => $data["name"],
                        "text" => $data["text"],
                        "href" => $data["href"],
                        "route" => $data["route"],
                    ];

                    // If a specific order was specified
                    if (array_key_exists("order", $data))
                    {
                        $sublinkData["order"] = $data["order"];

                        $this->moveSublinksDownForLinkFrom($link, $data["order"]);
                    }
                    // If no order was specified
                    else
                    {
                        $sublinkData["order"] = $link->sublinks->count() + 1;
                    }

                    // Create the sublink
                    $sublink = AdminNavigationLink::create($sublinkData);
                }
            }
        }
    }

    /**
     * Move down headers from given order
     * To make place for a header we just placed at the given order
     * 
     * @param       integer                         The order index to start moving the headers down from
     * @return      void
     */
    private function moveDownHeadersFrom($order)
    {
        // Grab all headers that should be moved down
        $headers = AdminNavigationHeader::where("order", ">=", $order)->get();

        // If there are headers to process
        if ($headers->count() > 0)
        {
            // Loop through each of them
            foreach ($headers as $header)
            {
                // Move down the header by one and save the changes
                $header->order -= 1;
                $header->save();
            }
        }
    }

    /**
     * Move links down for header from
     * 
     * @param       AdminNavigationHeader           The header we want to move the links down from
     * @param       integer                         The order to start moving links down at
     * @return      void
     */
    private function moveLinksDownForHeaderFrom(AdminNavigationHeader $header, $order)
    {
        // Find the links we should move down
        $links = AdminNavigationLink::where("admin_navigation_header_id", $header->id)->where("order", ">=", $order)->get();

        // If we found some links
        if ($links->count() > 0)
        {
            // Loop through all of them
            foreach ($links as $link)
            {
                // And update the order
                $link->order -= 1;
                $link->save();
            }
        }
    }

    /**
     * Move sublinks down for link from (given order)
     * 
     * @param       AdminNavigationLink                 The link we want to move the sublinks of
     * @param       integer                             Order to start moving down from
     * @return      void
     */
    private function moveSublinksDownForLinkFrom(AdminNavigationLink $link, $order)
    {
        if ($link->sublinks->count() > 0)
        {
            foreach ($link->sublinks as $sublink)
            {
                if ($sublink->order >= $order)
                {
                    $sublink->order -= 1;
                    $sublink->save();
                }
            }
        }
    }
}