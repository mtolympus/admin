<?php

namespace Hermes\Admin\Http\Controllers;

use Illuminate\Support\Facades\View;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function __construct()
    {
        View::composer("partials.breadcrumbs", function($view) {
            $view->with("home_icon", '<i class="fas fa-tachometer-alt"></i>');
        });
    }
}