<?php

namespace Hermes\Admin\Http\Controllers\Api;

use Exception;

use App\Http\Controllers\Controller;

use Hermes\Admin\Models\AdminNavigationLink;
use Hermes\Admin\Models\AdminNavigationHeader;

use Hermes\Admin\Http\Requests\Api\NavigationSettings\Headers\MoveHeaderRequest;
use Hermes\Admin\Http\Requests\Api\NavigationSettings\Headers\CreateHeaderRequest;
use Hermes\Admin\Http\Requests\Api\NavigationSettings\Headers\UpdateHeaderRequest;
use Hermes\Admin\Http\Requests\Api\NavigationSettings\Headers\DeleteHeaderRequest;
use Hermes\Admin\Http\Requests\Api\NavigationSettings\Links\MoveLinkRequest;
use Hermes\Admin\Http\Requests\Api\NavigationSettings\Links\CreateLinkRequest;
use Hermes\Admin\Http\Requests\Api\NavigationSettings\Links\UpdateLinkRequest;
use Hermes\Admin\Http\Requests\Api\NavigationSettings\Links\DeleteLinkRequest;
use Hermes\Admin\Http\Requests\Api\NavigationSettings\Sublinks\MoveSublinkRequest;
use Hermes\Admin\Http\Requests\Api\NavigationSettings\Sublinks\CreateSublinkRequest;
use Hermes\Admin\Http\Requests\Api\NavigationSettings\Sublinks\UpdateSublinkRequest;
use Hermes\Admin\Http\Requests\Api\NavigationSettings\Sublinks\DeleteSublinkRequest;

class NavigationSettingsApiController extends Controller
{
    // --------------------------------------------------
    // Headers
    // --------------------------------------------------

    public function postMoveHeader(MoveHeaderRequest $request)
    {
        try
        {
            // Grab the header we're moving
            $header = AdminNavigationHeader::find($request->admin_navigation_header_id);

            // Switch between the possible directions
            switch ($request->direction)
            {
                // If we're moving the header upwards
                case "up":

                    // Determine the order of the 'other' header (in this case, the header directly above the header we're moving up)
                    $otherHeaderOrder = $header->order - 1;

                    // Grab the other header (the header currently above the one we're moving up)
                    $otherHeader = AdminNavigationHeader::where("order", $otherHeaderOrder)->first();
                    if (!$otherHeader) throw new Exception("Could not find the other header (the one we should move down)");

                    // Update the header (move it up)
                    $header->order -= 1;
                    $header->save();

                    // Update the other header (move it down)
                    $otherHeader->order += 1;
                    $otherHeader->save();

                break;

                // If we're moving the header downwards
                case "down":

                    // Determine order of 'other' header (the header directly below the one we want to move down)
                    $otherHeaderOrder = $header->order + 1;

                    // Grab the other header
                    $otherHeader = AdminNavigationHeader::where("order", $otherHeaderOrder)->first();
                    if (!$otherHeader) throw new Exception("Could not find the other header (the one we should move up)");

                    // Update the header (move it down)
                    $header->order += 1;
                    $header->save();

                    // Update the other header (move it up)
                    $otherHeader->order -= 1;
                    $otherHeader->save();

                break;

                // If a weird direction was given
                default:
                    throw new Exception("Could not move the header because an unknown direction was given: '".$request->direction."'");
                break;
            }

            // Jaj
            return response()->json(["status" => "success"]);
        }
        catch (Exception $e)
        {
            return response()->json([
                "status" => "error",
                "error" => $e->getMessage()
            ]);
        }
    }

    public function postCreateHeader(CreateHeaderRequest $request)
    {
        try
        {
            $header = AdminNavigationHeader::create([
                "order" => $request->order,
                "name" => $request->name,
                "text" => $request->text
            ]);

            return response()->json(["status" => "success"]);
        }
        catch (Exception $e)
        {
            return response()->json([
                "status" => "error",
                "error" => $e->getMessage()
            ]);
        }
    }

    public function postEditHeader(UpdateHeaderRequest $request)
    {
        try
        {
            // Grab the header we want to update
            $header = AdminNavigationHeader::find($request->admin_navigation_header_id);
            
            // Update the header
            $header->name = $request->name;
            $header->text = $request->text;
            $header->save();
            
            return response()->json(["status" => "success"]);
        }
        catch (Exception $e)
        {
            return response()->json([
                "status" => "error",
                "error" => $e->getMessage()
            ]);
        }
    }

    public function postDeleteHeader(DeleteHeaderRequest $request)
    {
        try
        {
            // Grab the header we want to delete
            $header = AdminNavigationHeader::find($request->admin_navigation_header_id);   

            // Delete the fucker
            $header->delete();

            return response()->json(["status" => "success"]);
        }
        catch (Exception $e)
        {
            return response()->json([
                "status" => "error",
                "error" => $e->getMessage()
            ]);
        }
    }

    // --------------------------------------------------
    // Links
    // --------------------------------------------------
    
    public function postMoveLink(MoveLinkRequest $request)
    {
        try
        {
            // Grab the link we're moving
            $link = AdminNavigationLink::find($request->admin_navigation_link_id);

            // Switch between the possible directions
            switch ($request->direction)
            {
                // If we're moving the header upwards
                case "up":

                    // Grab the other header (the header currently above the one we're moving up)
                    $otherLink = AdminNavigationLink::where("order", ($link->order - 1))->first();
                    if (!$otherLink) throw new Exception("Could not find the other link (the one we should move down)");

                    // Update the link
                    $link->order -= 1;
                    $link->save();

                    // Update the other link
                    $otherLink->order += 1;
                    $otherLink->save();

                break;

                // If we're moving the link downwards
                case "down":

                    // Grab the other link
                    $otherLink = AdminNavigationLink::where("order", ($link->order + 1))->first();
                    if (!$otherLink) throw new Exception("Could not find the other link (the one we should move up)");

                    // Update the link
                    $link->order += 1;
                    $link->save();

                    // Update the other link
                    $otherLink->order -= 1;
                    $otherLink->save();

                break;

                // If a weird direction was given
                default:
                    throw new Exception("Could not move the link because an unknown direction was given: '".$request->direction."'");
                break;
            }

            return response()->json(["status" => "success"]);
        }
        catch (Exception $e)
        {
            return response()->json([
                "status" => "error",
                "error" => $e->getMessage()
            ]);
        }
    }

    public function postCreateLink(CreateLinkRequest $request)
    {
        try
        {
            // Create the link
            $link = AdminNavigationLink::create([
                "admin_navigation_header_id" => $request->admin_navigation_header_id,
                "order" => $request->order,
                "name" => $request->name,
                "text" => $request->text,
                "icon" => $request->icon,
                "href" => $request->href,
                "route" => $request->route 
            ]);

            return response()->json(["status" => "success"]);
        }
        catch (Exception $e)
        {
            return response()->json([
                "status" => "error",
                "error" => $e->getMessage()
            ]);
        }
    }

    public function postEditLink(UpdateLinkRequest $request)
    {
        try
        {
            // Grab the link
            $link = AdminNavigationLink::find($request->admin_navigation_link_id);

            // Update the link
            $link->name = $request->name;
            $link->text = $request->text;
            $link->icon = $request->icon;
            $link->href = $request->href;
            $link->route = $request->route;
            $link->save();

            return response()->json(["status" => "success"]);
        }
        catch (Exception $e)
        {
            return response()->json([
                "status" => "error",
                "error" => $e->getMessage()
            ]);
        }
    }

    public function postDeleteLink(DeleteLinkRequest $request)
    {
        try
        {
            // Grab the link
            $link = AdminNavigationLink::find($request->admin_navigation_link_id);

            // Delete it
            $link->delete();

            return response()->json(["status" => "success"]);
        }
        catch (Exception $e)
        {
            return response()->json([
                "status" => "error",
                "error" => $e->getMessage()
            ]);
        }
    }

    // --------------------------------------------------
    // Sublinks
    // --------------------------------------------------
    
    public function postMoveSublink(MoveSublinkRequest $request)
    {
        try
        {
            // Grab the link we're moving
            $sublink = AdminNavigationLink::find($request->admin_navigation_link_id);

            // Switch between the possible directions
            switch ($request->direction)
            {
                // If we're moving the header upwards
                case "up":

                    // Grab the other header (the header currently above the one we're moving up)
                    $otherSublink = AdminNavigationLink::where("admin_navigation_link_id", $sublink->admin_navigation_link_id)->where("order", ($sublink->order - 1))->first();
                    if (!$otherSublink) throw new Exception("Could not find the other link (the one we should move down)");

                    // Update the link
                    $sublink->order -= 1;
                    $sublink->save();

                    // Update the other link
                    $otherSublink->order += 1;
                    $otherSublink->save();

                break;

                // If we're moving the link downwards
                case "down":

                    // Grab the other link
                    $otherSublink = AdminNavigationLink::where("admin_navigation_link_id", $sublink->admin_navigation_link_id)->where("order", ($sublink->order + 1))->first();
                    if (!$otherSublink) throw new Exception("Could not find the other sublink (the one we should move up)");

                    // Update the link
                    $sublink->order += 1;
                    $sublink->save();

                    // Update the other link
                    $otherSublink->order -= 1;
                    $otherSublink->save();

                break;

                // If a weird direction was given
                default:
                    throw new Exception("Could not move the sublink because an unknown direction was given: '".$request->direction."'");
                break;
            }

            return response()->json(["status" => "success"]);
        }
        catch (Exception $e)
        {
            return response()->json([
                "status" => "error",
                "error" => $e->getMessage()
            ]);
        }
    }

    public function postCreateSublink(CreateSublinkRequest $request)
    {
        try
        {
            // Create the sublink
            $sublink = AdminNavigationLink::create([
                "admin_navigation_link_id" => $request->admin_navigation_link_id,
                "order" => $request->order,
                "name" => $request->name,
                "text" => $request->text,
                "href" => $request->href,
                "route" => $request->route
            ]);
            
            return response()->json(["status" => "success"]);
        }
        catch (Exception $e)
        {
            return response()->json([
                "status" => "error",
                "error" => $e->getMessage()
            ]);
        }
    }

    public function postEditSublink(UpdateSublinkRequest $request)
    {
        try
        {
            // Grab the sublink
            $sublink = AdminNavigationLink::find($request->admin_navigation_link_id);

            // Update the sublink
            $sublink->name = $request->name;
            $sublink->text = $request->text;
            $sublink->href = $request->href;
            $sublink->route = $request->route;
            $sublink->save();

            return response()->json(["status" => "success"]);
        }
        catch (Exception $e)
        {
            return response()->json([
                "status" => "error",
                "error" => $e->getMessage()
            ]);
        }
    }

    public function postDeleteSublink(DeleteSublinkRequest $request)
    {
        try
        {
            // Grab the sublink
            $sublink = AdminNavigationLink::find($request->admin_navigation_link_id);

            // Delete the sublink
            $sublink->delete();
        
            return response()->json(["status" => "success"]);
        }
        catch (Exception $e)
        {
            return response()->json([
                "status" => "error",
                "error" => $e->getMessage()
            ]);
        }
    }
}