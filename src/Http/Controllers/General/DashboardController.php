<?php

namespace Hermes\Admin\Http\Controllers\General;

use Hermes\Admin\Http\Controllers\AdminController;

class DashboardController extends AdminController
{
    public function getView()
    {
        return view("admin::pages.dashboard.view");
    }
}