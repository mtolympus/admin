<?php

namespace Hermes\Admin\Http\Controllers\General;

use Settings;
use Uploader;

use Hermes\Admin\Models\AdminNavigationLink;
use Hermes\Admin\Models\AdminNavigationHeader;

use Hermes\Admin\Http\Controllers\AdminController;

use Hermes\Admin\Http\Requests\Settings\UpdateDashboardSettingsRequest;
use Hermes\Admin\Http\Requests\Settings\UpdateTemplateSettingsRequest;
use Hermes\Admin\Http\Requests\Settings\UpdateNavigationSettingsRequest;
use Hermes\Admin\Http\Requests\Settings\UpdateAuthenticationSettingsRequest;

class SettingsController extends AdminController
{
    public function __construct()
    {
        parent::__construct();
    }

    // ----------------------------------------------------
    // Dashboard settings
    // ----------------------------------------------------

    public function getDashboardSettings()
    {
        return view("admin::pages.settings.dashboard-settings", []);
    }

    public function postDashboardSetings(UpdateDashboardSettingsRequest $request) 
    {
        
    }

    // ----------------------------------------------------
    // Template settings
    // ----------------------------------------------------
    
    public function getTemplateSettings()
    {
        $setting_logo_image  = Settings::getSetting("logo_image", "admin_template");
        $setting_logo_text   = Settings::getSetting("logo_text", "admin_template");
        $setting_footer_text = Settings::getSetting("footer_text", "admin_template");

        return view("admin::pages.settings.template-settings", [
            "settingLogoImage" => $setting_logo_image,
            "settingLogoText" => $setting_logo_text,
            "settingFooterText" => $setting_footer_text,
            "oldInput" => collect([
                "logo_text" => old("logo_text"),
                "footer_text" => old("footer_text")
            ])            
        ]);
    }

    public function postTemplateSettings(UpdateTemplateSettingsRequest $request)
    {
        // If we received a new logo image
        if ($request->hasFile("logo_image"))
        {
            // Upload the new image
            $logo_image_path = Uploader::upload($request->file("logo_image"), "images/admin/logos");

            // And save the uploaded image's path on the setting
            Settings::setByName("logo_image", $logo_image_path, "admin_template");
        }

        // Save all of the simpler fields
        Settings::setByName("logo_text", $request->logo_text, "admin_template");
        Settings::setByName("footer_text", $request->footer_text, "admin_template");

        // Flash message and redirect back
        flash("Successfully saved changes to the template settings.")->success();
        return redirect()->route("admin.settings.template");
    }

    // ----------------------------------------------------
    // Navigation settings
    // ----------------------------------------------------
    
    public function getNavigationSettings()
    {
        $settings = $this->getHeadersForNavigationSettings();
        return view("admin::pages.settings.navigation-settings", [
            "headers" => $settings
        ]);
    }

    private function getHeadersForNavigationSettings()
    {
        $out = [];

        // Grab all of the navigation headers from the database
        $headers = AdminNavigationHeader::all();
        $headers->load("links");
        $headers->load("links.sublinks");

        return $headers;
    }

    // ----------------------------------------------------
    // Authentication settings
    // ----------------------------------------------------
    
    public function getAuthenticationSettings()
    {
        $setting_require_email_verification  = Settings::getSetting("require_email_verification", "admin_auth");
        $setting_allow_registration         = Settings::getSetting("allow_registration", "admin_auth");
        $setting_allow_account_recovery     = Settings::getSetting("allow_account_recovery", "admin_auth");
        $setting_enable_two_factor_auth     = Settings::getSetting("enable_two_factor", "admin_auth");
        
        $settings = collect([
            "require_email_verification" => Settings::getSetting("require_email_verification", "admin_auth"),
            "allow_registration" => Settings::getSetting("allow_registration", "admin_auth"),
            "allow_account_recovery" => Settings::getSetting("allow_account_recovery", "admin_auth"),
            "enable_two_factor_auth" => Settings::getSetting("enable_two_factor", "admin_auth")
        ]);

        return view("admin::pages.settings.authentication-settings", [
            "settingRequireEmailVerification" => $setting_require_email_verification,
            "settingAllowRegistration" => $setting_allow_registration,
            "settingAllowAccountRecovery" => $setting_allow_account_recovery,
            "settingEnableTwoFactorAuth" => $setting_enable_two_factor_auth,
            "oldInput" => collect([
                "require_email_verification" => old("require_email_verification"),
                "allow_registration" => old("allow_registration"),
                "allow_account_recovery" => old("allow_account_recovery"),
                "enable_two_factor_auth" => old("enable_two_factor_auth")
            ])
        ]);
    }

    public function postAuthenticationSettings(UpdateAuthenticationSettingsRequest $request)
    {
        // Set all of the data!
        Settings::setByName("require_email_verification", $this->convertStringToBoolean($request->require_email_verification), "admin_auth");
        Settings::setByName("allow_registration", $this->convertStringToBoolean($request->allow_registration), "admin_auth");
        Settings::setByName("allow_account_recovery", $this->convertStringToBoolean($request->allow_account_recovery), "admin_auth");
        Settings::setByName("enable_two_factor", $this->convertStringToBoolean($request->enable_two_factor_auth), "admin_auth");

        // Flash message and redirect back
        flash("Successfully saved changes to the authentication settings.")->success();
        return redirect()->route("admin.settings.auth");
    }

    private function convertStringToBoolean($val)
    {
        return $val === "true" ? true : false;
    }
}