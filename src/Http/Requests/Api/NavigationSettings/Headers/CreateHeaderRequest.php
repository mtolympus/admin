<?php

namespace Hermes\Admin\Http\Requests\Api\NavigationSettings\Headers;

use Illuminate\Foundation\Http\FormRequest;

class CreateHeaderRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            "order" => "required",
            "name" => "required",
            "text" => "required"
        ];
    }
}
