<?php

namespace Hermes\Admin\Http\Requests\Api\NavigationSettings\Headers;

use Illuminate\Foundation\Http\FormRequest;

class DeleteHeaderRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            "admin_navigation_header_id" => "required|integer|exists:admin_navigation_headers,id"
        ];
    }
}
