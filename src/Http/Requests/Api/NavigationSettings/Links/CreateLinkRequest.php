<?php

namespace Hermes\Admin\Http\Requests\Api\NavigationSettings\Links;

use Illuminate\Foundation\Http\FormRequest;

class CreateLinkRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            "admin_navigation_header_id" => "required|integer|exists:admin_navigation_headers,id",
            "order" => "required|integer",
            "name" => "required",
            "text" => "required",
            "icon" => "nullable",
            "href" => "required",
            "route" => "nullable"
        ];
    }
}
