<?php

namespace Hermes\Admin\Http\Requests\Api\NavigationSettings\Links;

use Illuminate\Foundation\Http\FormRequest;

class DeleteLinkRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            "admin_navigation_link_id" => "required|integer|exists:admin_navigation_links,id",
        ];
    }
}
