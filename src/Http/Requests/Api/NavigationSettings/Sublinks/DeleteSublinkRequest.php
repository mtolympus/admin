<?php

namespace Hermes\Admin\Http\Requests\Api\NavigationSettings\Sublinks;

use Illuminate\Foundation\Http\FormRequest;

class DeleteSublinkRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            "admin_navigation_link_id" => "required|integer|exists:admin_navigation_links,id",
        ];
    }
}
