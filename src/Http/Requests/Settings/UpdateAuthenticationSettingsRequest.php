<?php

namespace Hermes\Admin\Http\Requests\Settings;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAuthenticationSettingsRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            "require_email_verification" => "required",
            "allow_registration" => "required",
            "allow_account_recovery" => "required",
            "enable_two_factor_auth" => "required",
        ];
    }

    public function message()
    {
        return [];
    }
}
