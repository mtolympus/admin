<?php

namespace Hermes\Admin\Http\Requests\Settings;

use Illuminate\Foundation\Http\FormRequest;

class UpdateNavigationSettingsRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            
        ];
    }

    public function message()
    {
        return [
            
        ];
    }
}
