<?php

namespace Hermes\Admin\Http\Requests\Settings;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTemplateSettingsRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            "logo_image" => "nullable|image|max:20000",
            "logo_text" => "required",
            "footer_text" => "required"
        ];
    }

    public function message()
    {
        return [
            
        ];
    }
}
