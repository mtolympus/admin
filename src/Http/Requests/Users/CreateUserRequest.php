<?php

namespace Hermes\Admin\Http\Requests\Users;

use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            
        ];
    }

    public function message()
    {
        return [
            
        ];
    }
}
