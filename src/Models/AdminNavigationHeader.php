<?php

namespace Hermes\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class AdminNavigationHeader extends Model
{
    protected $table = "admin_navigation_headers";
    protected $guarded = ["id", "created_at", "updated_at"];
    protected $fillable = [
        "order",
        "name",
        "text"
    ];

    //
    // Relationships
    //

    public function links()
    {
        return $this->hasMany("Hermes\Admin\Models\AdminNavigationLink", "admin_navigation_header_id", "id");
    }
}