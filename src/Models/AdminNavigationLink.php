<?php

namespace Hermes\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class AdminNavigationLink extends Model
{
    protected $table = "admin_navigation_links";
    protected $guarded = ["id", "created_at", "updated_at"];
    protected $fillable = [
        "admin_navigation_header_id",
        "admin_navigation_link_id",
        "order",
        "name",
        "text",
        "href",
        "icon",
        "route",
    ];
    
    //
    // Relationships
    //

    public function header()
    {
        return $this->belongsTo("Hermes\Admin\Models\AdminNavigationHeader", "id", "admin_navigation_header_id");
    }

    public function parentLink()
    {
        return $this->belongsTo("Hermes\Admin\Models\AdminNavigationLink", "id", "admin_navigation_link_id");
    }

    public function sublinks()
    {
        return $this->hasMany("Hermes\Admin\Models\AdminNavigationLink", "admin_navigation_link_id", "id");
    }
}