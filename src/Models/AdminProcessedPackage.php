<?php

namespace Hermes\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class AdminProcessedPackage extends Model
{
    protected $table = "admin_processed_packages";
    protected $guarded = ["id", "created_at", "updated_at"];
    protected $fillable = [
        "package_name"
    ];
}