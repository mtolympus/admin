<?php

namespace Hermes\Admin\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class AdminRouteServiceProvider extends ServiceProvider
{
    protected $namespace = "Hermes\Admin\Http\Controllers";

    public function boot() 
    {
        parent::boot();
    }

    public function map()
    {
        $this->mapApiRoutes();
        $this->mapWebRoutes();
    }

    protected function mapWebRoutes()
    {
        Route::middleware("web")->namespace($this->namespace)->group(__DIR__."/../Routes/web.php");
    }

    protected function mapApiRoutes()
    {
        Route::prefix("api")->middleware("api")->namespace($this->namespace)->group(__DIR__."/../Routes/api.php");
    }
}