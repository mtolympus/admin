<?php

namespace Hermes\Admin\Providers;

use Settings;

use Hermes\Admin\Services\AdminManager;
use Hermes\Admin\Services\NavigationManager;

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AdminServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Compose the admin layout
        View::composer("admin::layouts.master", function($view) {
            // The 'base' page title of each admin panel page
            $view->with("page_title", config("admin.page_title"));
            // Logo text & image
            $view->with("logoImage", Settings::get("logo_image", "admin_template"));
            $view->with("logoText", Settings::get("logo_text", "admin_template"));
            // Footer text
            $view->with("footerText", Settings::get("footer_text", "admin_template"));
        });

        $this->app->booted(function () {
            View::composer("admin::layouts.master", function($view) {
                $view->with("navigationData", app()->make('navigationManager')->getNavigationData());
            });
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Register the package's route service provider which will register all routes
        $this->app->register(AdminRouteServiceProvider::class);

        // Register the Admin Manager service as a singleton
        $this->app->singleton('adminManager', function() {
            return new AdminManager;
        });

        // Register the Navigation Manager service as a singleton
        $this->app->singleton('navigationManager', function() {
            return new NavigationManager;
        });
        
        // Bootstrap all of the package's components
        $this->registerConfig();
        $this->registerViews();
        $this->registerAssets();
        $this->registerDatabase();
        $this->registerBreadcrumbs();
    }

    private function registerConfig()
    {
        // Merge the config file with the application's configuration
        $this->mergeConfigFrom(__DIR__."/../Config/config.php", "admin");

        // Setup publishing of the config file
        $this->publishes([__DIR__."/../Config/config.php" => config_path("admin.php")]);

        // Setup publishing of the configured breadcrumbs config file
        $this->publishes([__DIR__."/../Config/breadcrumbs.php" => config_path("breadcrumbs.php")]);
    }

    private function registerViews()
    {
        // Register where to load the views of this package from
        $this->loadViewsFrom(__DIR__ . "/../Resources/views", "admin");
        
        // Setup publishing of the views
        $this->publishes([__DIR__ . "/../Resources/views" => resource_path("views/vendor/admin")], "views");
    }

    private function registerAssets()
    {
        // Setup publishing of the sass files
        $this->publishes([], "assets");

        // Setup publishing of the vue components
        $this->publishes([], "assets");

        $this->publishes([
            __DIR__."/../Resources/assets/sass" => resource_path("sass"),
            __DIR__."/../Resources/assets/js/components" => resource_path("js/components/admin_module"),
            __DIR__."/../Resources/assets/images/logo.png" => storage_path("app/public/images/admin/logo.png"),
        ], "assets");
    }

    private function registerDatabase()
    {
        // Load the package's migrations
        $this->loadMigrationsFrom(__DIR__."/../Database/migrations");
        
        // Setup publishing of the migrations allowing custom overrides
        $this->publishes([__DIR__."/../Database/migrations" => database_path("migrations")], "database");

        // Setup publishing of the seeders
        $this->publishes([__DIR__."/../Database/seeds" => database_path("seeds")], "database");
    }

    private function registerBreadcrumbs()
    {
        // Publish the default breadcrumb directories & files to the app's routes directory
        $this->publishes([__DIR__ . "/../Routes/breadcrumbs" => base_path('routes/breadcrumbs')], "breadcrumbs");


    }
}