<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ $page_title }} @yield('page_title')</title>

        {{-- Laravel Mix - CSS File --}}
        <link rel="stylesheet" href="{{ mix('css/admin.css') }}">

        {{-- Font Awesome Icons --}}
        <script defer src="https://use.fontawesome.com/releases/v5.5.0/js/all.js" integrity="sha384-GqVMZRt5Gn7tB9D9q7ONtcp4gtHIUEW/yG7h98J7IpE3kpi+srfFyyB/04OV6pG0" crossorigin="anonymous"></script>

    </head>
    <body>

        <!-- App -->
        <v-app id="app">

            <!-- Topnav -->
            <div id="topnav" class="elevation-1">

                <!-- Logo wrapper -->
                <div id="topnav-logo">
                    <!-- Logo -->
                    <img id="logo" src="{{ $logoImage }}" />
                    <!-- Logo text -->
                    <div id="logo-text">{{ $logoText }}</div>
                </div>

                <!-- Topnav wrapper -->
                <div id="topnav-content">
                    Topnav here
                </div>

            </div>
            
            <!-- Content -->
            <div id="content-wrapper">

                <!-- Sidebar -->
                <div id="navigation-wrapper">
                    
                    <!-- User card -->
                    <div id="user-card">
                        <div id="user-card__avatar">
                            <div id="avatar"></div>
                        </div>
                        <div id="user-card__text">
                            <div id="user-card__name">Nick Verheijen</div>
                            <div id="user-card__roles">
                                <div class="role">Administrator</div>
                                <div class="role">Trainee</div>
                            </div>
                        </div>
                    </div>

                    <!-- Links -->
                    <div id="navigation-links">
                        <admin-navigation 
                            :data="{{ $navigationData->toJson() }}">
                        </admin-navigation>
                    </div>

                    <!-- Spacer -->
                    <div id="navigation-spacer"></div>

                    <!-- Logout button -->
                    <div id="navigation-logout">
                        <a id="logout-button" href="{{ route('auth.logout') }}">
                            <i class="fas fa-sign-out-alt"></i>
                            Logout
                        </a>
                    </div>

                </div>

                <!-- Content area -->
                <div id="content-area">

                    <!-- Page content's -->
                    <div id="content">
                        @yield("content")
                    </div>

                    <!-- Page footer -->
                    <div id="footer">{!! $footerText !!}</div>

                </div>

            </div>

        </v-app>

        {{-- Laravel Mix - JS File --}}
        <script src="{{ mix('js/app.js') }}"></script>

    </body>
</html>
