@extends("admin::layouts.master")

@section("content")

    <div class="wrapper">

        <!-- Breadcrumbs -->
        {!! Breadcrumbs::render("admin.dashboard") !!}

        <!-- Header -->
        <div class="page-header">
            <div class="page-header__text">
                <div class="page-header__title">Hey Nick!</div>
                <div class="page-header__subtitle">Welcome to your personal mount Olympus.</div>
            </div>
        </div>
        
        <!-- Feedback -->
        @include("admin::partials.feedback")

        <!-- Content -->
        <div class="content-card elevation-1">

            Stuff and things here

        </div>

    </div>

@stop