@extends("admin::layouts.master")

@section("content")

    <div class="wrapper">

        <!-- Breadcrumbs -->
        {!! Breadcrumbs::render("admin.settings.auth") !!}

        <!-- Header -->
        <div class="page-header">
            <div class="page-header__text">
                <div class="page-header__title">Authentication settings</div>
                <div class="page-header__subtitle">Define how the auth package should behave.</div>
            </div>
        </div>
        
        <!-- Feedback -->
        @include("admin::partials.feedback")

        <!-- Content -->
        <div class="content-card elevation-1">
            <form action="{{ route('admin.settings.auth.post') }}" method="post">
                {{ csrf_field() }}

                <auth-settings
                    :setting-require-email-verification="{{ $settingRequireEmailVerification->toJson() }}"
                    :setting-allow-registration="{{ $settingAllowRegistration->toJson() }}"
                    :setting-allow-account-recovery="{{ $settingAllowAccountRecovery->toJson() }}"
                    :setting-enable-two-factor-auth="{{ $settingEnableTwoFactorAuth }}"
                    :old-input="{{ $oldInput->toJson() }}">
                </auth-settings>

                <div class="controls">
                    <div class="controls-right">
                        <v-btn color="success" type="submit">
                            <i class="far fa-save"></i>
                            Save changes
                        </v-btn>
                    </div>
                </div>
                
            </form>
        </div>

    </div>

@stop