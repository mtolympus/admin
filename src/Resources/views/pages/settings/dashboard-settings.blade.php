@extends("admin::layouts.master")

@section("content")

    <div class="wrapper">

        <!-- Breadcrumbs -->
        {!! Breadcrumbs::render("admin.settings.dashboard") !!}

        <!-- Header -->
        <div class="page-header">
            <div class="page-header__text">
                <div class="page-header__title">Dashboard settings</div>
                <div class="page-header__subtitle">Define what should be displayed on your dashboard.</div>
            </div>
        </div>
        
        <!-- Feedback -->
        @include("admin::partials.feedback")

        <!-- Content -->
        <div class="content-card elevation-1">

            Stuff and things here

        </div>

    </div>

@stop