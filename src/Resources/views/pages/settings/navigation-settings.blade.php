@extends("admin::layouts.master")

@section("content")

    <div class="wrapper">

        <!-- Breadcrumbs -->
        {!! Breadcrumbs::render("admin.settings.navigation") !!}

        <!-- Header -->
        <div class="page-header">
            <div class="page-header__text">
                <div class="page-header__title">Navigation settings</div>
                <div class="page-header__subtitle">Define what's displayed in the navigation.</div>
            </div>
        </div>
        
        <!-- Feedback -->
        @include("admin::partials.feedback")

        <!-- Content -->
        <div class="content-card elevation-1">
            <navigation-settings
                :data="{{ $headers->toJson() }}"
                base-url="{{ url('/') }}">
            </navigation-settings>
        </div>

    </div>

@stop