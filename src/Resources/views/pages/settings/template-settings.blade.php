@extends("admin::layouts.master")

@section("content")

    <div class="wrapper">

        <!-- Breadcrumbs -->
        {!! Breadcrumbs::render("admin.settings.template") !!}

        <!-- Header -->
        <div class="page-header">
            <div class="page-header__text">
                <div class="page-header__title">Template settings</div>
                <div class="page-header__subtitle">Define how the template should behave and what's displayed.</div>
            </div>
        </div>
        
        <!-- Feedback -->
        @include("admin::partials.feedback")

        <!-- Content -->
        <div class="content-card elevation-1">
            <form action="{{ route('admin.settings.template.post') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}

                <template-settings
                    :setting-logo-image="{{ $settingLogoImage->toJson() }}"
                    :setting-logo-text="{{ $settingLogoText->toJson() }}"
                    :setting-footer-text="{{ $settingFooterText->toJson() }}"
                    :old-input="{{ $oldInput->toJson() }}">
                </template-settings>
                
                <div class="controls">
                    <div class="controls-right">
                        <v-btn color="success" type="submit">
                            <i class="far fa-save"></i>
                            Save changes
                        </v-btn>
                    </div>
                </div>
                
            </form>
        </div>

    </div>

@stop