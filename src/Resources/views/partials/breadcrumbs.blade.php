@if (count($breadcrumbs) > 0)
    <div id="breadcrumbs-wrapper">
        <ul id="breadcrumbs">
            @foreach ($breadcrumbs as $breadcrumb)
                @if ($breadcrumb->url && $loop->remaining)
                    <li class="breadcrumb-item">
                        <a href="{{ $breadcrumb->url }}">
                            @if ($loop->first)
                                @if (isset($home_icon))
                                    {!! $home_icon !!}
                                @else
                                    <i class="fas fa-home"></i>
                                @endif
                            @endif
                            {{ $breadcrumb->title }}
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <v-icon small>chevron_right</v-icon>
                    </li>
                @else
                    <li class="breadcrumb-item active">
                        @if ($loop->first)
                            @if (isset($home_icon))
                                {!! $home_icon !!}
                            @else
                                <i class="fas fa-home"></i>
                            @endif
                        @endif
                        {{ $breadcrumb->title }}
                    </li>
                @endif
            @endforeach
        </ul>
    </div>
@endif