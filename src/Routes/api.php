<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
*/

Route::group(["prefix" => "admin"], function() {
    
    // --------------------------------------------------------
    // Navigation settings
    // --------------------------------------------------------

    Route::group(["prefix" => "navigation-settings"], function() {

        // Headers
        Route::group(["prefix" => "headers"], function() {
            // Move
            Route::post("move", "Api\NavigationSettingsApiController@postMoveHeader")->name("api.navigation-settings.header.move");
            // Create
            Route::post("create", "Api\NavigationSettingsApiController@postCreateHeader")->name("api.navigation-settings.header.create");
            // Edit
            Route::post("edit", "Api\NavigationSettingsApiController@postEditHeader")->name("api.navigation-settings.header.edit");
            // Delete
            Route::post("delete", "Api\NavigationSettingsApiController@postDeleteHeader")->name("api.navigation-settings.header.delete");
        });

        // Links 
        Route::group(["prefix" => "links"], function() {
            // Move
            Route::post("move", "Api\NavigationSettingsApiController@postMoveLink")->name("api.navigation-settings.link.move");
            // Create
            Route::post("create", "Api\NavigationSettingsApiController@postCreateLink")->name("api.navigation-settings.link.create");
            // Edit
            Route::post("edit", "Api\NavigationSettingsApiController@postEditLink")->name("api.navigation-settings.link.edit");
            // Delete
            Route::post("delete", "Api\NavigationSettingsApiController@postDeleteLink")->name("api.navigation-settings.link.delete");
        });

        // Sublinks
        Route::group(["prefix" => "sublinks"], function() {
            // Move
            Route::post("move", "Api\NavigationSettingsApiController@postMoveSublink")->name("api.navigation-settings.sublink.move");
            // Create
            Route::post("create", "Api\NavigationSettingsApiController@postCreateSublink")->name("api.navigation-settings.sublink.create");
            // Edit
            Route::post("edit", "Api\NavigationSettingsApiController@postEditSublink")->name("api.navigation-settings.sublink.edit");
            // Delete
            Route::post("delete", "Api\NavigationSettingsApiController@postDeleteSublink")->name("api.navigation-settings.sublink.delete");
        });

    });

});

// --------------------------------------------------------
// 
// --------------------------------------------------------