<?php

// -----------------------------------------------------------------
// Dashboard
// -----------------------------------------------------------------

Breadcrumbs::for("admin.dashboard", function($t) {
    $t->push("Dashboard", route("admin.dashboard"));
});

// -----------------------------------------------------------------
// Auth
// -----------------------------------------------------------------

Breadcrumbs::for("admin.users", function($t) {
    $t->parent("admin.dashboard");
    $t->push("Manage users", route("admin.users"));
});
Breadcrumbs::for("admin.users.view", function($t, $user) {
    $t->parent("admin.users");
    $t->push($user->name, route("admin.users.view", $user->slug));
});
Breadcrumbs::for("admin.users.create", function($t) {
    $t->parent("admin.users");
    $t->push("Create new user", route("admin.users.create"));
});
Breadcrumbs::for("admin.users.edit", function($t, $user) {
    $t->parent("admin.users.view", $user);
    $t->push("Edit user", route("admin.users.edit", $user->slug));
});
Breadcrumbs::for("admin.users.delete", function($t, $user) {
    $t->parent("admin.users.view", $user);
    $t->push("Delete user", route("admin.users.delete", $user->slug));
});

Breadcrumbs::for("admin.roles", function($t) {
    $t->parent("admin.dashboard");
    $t->push("Roles", route("admin.roles"));
});
Breadcrumbs::for("admin.roles.view", function($t, $role) {
    $t->parent("admin.roles");
    $t->puhs($role->name, route("admin.roles.view", $role->id));
});
Breadcrumbs::for("admin.roles.create", function($t) {
    $t->parent("admin.roles");
    $t->push("Create new role", route("admin.roles.create"));
});
Breadcrumbs::for("admin.roles.edit", function($t, $role) {
    $t->parent("admin.roles.view", $role);
    $t->push("Edit role", $role->id);
});
Breadcrumbs::for("admin.roles.delete", function($t, $role) {
    $t->parent("admin.users.view", $role);
    $t->push("Delete role", $role->id);
});

Breadcrumbs::for("admin.permissions", function($t) {
    $t->parent("admin.dashboard");
    $t->push("Permissions", route("admin.permissions"));
});

// -----------------------------------------------------------------
// Settings
// -----------------------------------------------------------------

Breadcrumbs::for("admin.settings.dashboard", function($t) {
    $t->parent("admin.dashboard");
    $t->push("Dashboard settings", route("admin.settings.dashboard"));
});
Breadcrumbs::for("admin.settings.template", function($t) {
    $t->parent("admin.dashboard");
    $t->push("Template settings", route("admin.settings.template"));
});
Breadcrumbs::for("admin.settings.navigation", function($t) {
    $t->parent("admin.dashboard");
    $t->push("Navigation settings", route("admin.settings.navigation"));
});
Breadcrumbs::for("admin.settings.auth", function($t) {
    $t->parent("admin.dashboard");
    $t->push("Authentication settings", route("admin.settings.auth"));
});