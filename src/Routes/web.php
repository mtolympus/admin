<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(["prefix" => "admin"], function() {

    // Dashboard
    Route::get("/", "General\DashboardController@getView")->name("admin.dashboard");
    
    // Manage settings
    Route::group(["prefix" => "settings"], function() {

        // Dashboard settings
        Route::get("dashboard", "General\SettingsController@getDashboardSettings")->name("admin.settings.dashboard");
        Route::post("dashboard", "General\SettingsController@postDashboardSettings")->name("admin.settings.dashboard.post");

        // Template settings
        Route::get("template", "General\SettingsController@getTemplateSettings")->name("admin.settings.template");
        Route::post("template", "General\SettingsController@postTemplateSettings")->name("admin.settings.template.post");

        // Navigation settings
        Route::get("navigation", "General\SettingsController@getNavigationSettings")->name("admin.settings.navigation");
        
        // Authentication settings
        Route::get("authentication", "General\SettingsController@getAuthenticationSettings")->name("admin.settings.auth");
        Route::post("authentication", "General\SettingsController@postAuthenticationSettings")->name("admin.settings.auth.post");
        
    });

});