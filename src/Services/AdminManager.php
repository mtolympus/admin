<?php

namespace Hermes\Admin\Services;

use Schema;
use NavigationManager;

use Hermes\Settings\Settings;
use Hermes\Auth\Services\PermissionManager;
use Hermes\Admin\Models\AdminProcessedPackage;


class AdminManager
{
    /**
     * Register a package
     * 
     * @param       string                          Name of the package we want to register
     * @param       array                           Configuration properties
     * @return      void
     */
    public function registerPackage(string $packageName, $callback)
    {
        // Determine if the database has been migrated, we need to do this because this code will be loaded instantly after package's 
        // get installed (including this package) and if we try to register packages when the database has not been migrated we get errors
        // and artisan breaks.
        $admin_tables = [
            "users", "roles", "permission_groups", "permissions",
            "admin_navigation_headers", "admin_navigation_links", "admin_processed_packages",
        ];
        foreach ($admin_tables as $tableName) {
            if (!Schema::hasTable($tableName)) {
                return;
            }
        }

        // If the package has not been processed yet
        if (!$this->packageHasBeenProcessed($packageName))
        {
            // Run the callback provided by whoever is registering a package
            $callback(new NavigationManager, new PermissionManager, new Settings);

            // Flag the package as processed
            $this->flagPackageAsProcessed($packageName);
        }
    }

    /**
     * Flag package as processed
     * 
     * @param       string                          Name of the package we want to flag as processed
     * @return      void
     */
    public function flagPackageAsProcessed(string $packageName)
    {
        // Attempt to grab the flag
        $flag = AdminProcessedPackage::where("package_name", $packageName)->first();

        // If the flag does not exist already
        if (!$flag)
        {
            // Create the flag!
            AdminProcessedPackage::create(["package_name" => $packageName]);
        }
    }

    /**
     * Package has been processed
     * 
     * @param       string                          Name of the package we want to check for existence
     * @return      boolean
     */
    public function packageHasBeenProcessed(string $packageName)
    {
        return AdminProcessedPackage::where("package_name", $packageName)->first() ? true : false;
    }
}