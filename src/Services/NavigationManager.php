<?php

namespace Hermes\Admin\Services;

use Illuminate\Support\Facades\Route;
use Hermes\Admin\Models\AdminNavigationLink;
use Hermes\Admin\Models\AdminNavigationHeader;

/**
 * Navigation Manager
 * 
 * This service is responsible for handling the generation of the admin panel's
 * navigation headers & links. Other packages will need to hook into the navigation
 * of the admin panel which this service provides methods for.
 * 
 * @version     1.0.0
 * @author      Nick Verheijen <verheijen.webdevelopment@gmail.com>
 */
class NavigationManager
{
    /**
     * The navigation header & link data
     * 
     * @param       array
     */
    private $data;

    /**
     * Constructor
     * 
     * @return      void
     */
    public function __construct()
    {

    }

    public function registerHeader()
    {

    }

    public function registerHeaders()
    {

    }

    public function registerLink()
    {

    }

    public function registerLinks()
    {

    }
    
    /**
     * Get navigation data (for navigation component)
     * 
     * @return      Illuminate\Support\Collection
     */
    public function getNavigationData()
    {
        $out = [];

        // Grab all of the headers from the database
        $headers = AdminNavigationHeader::all();
        
        // If there are headers to process
        if ($headers->count() > 0)
        {
            // Determine the currently active route
            $currentRouteName = Route::currentRouteName();

            // Sort the headers by order
            $headers = $headers->sortBy("order");

            // Loop through all of the headers
            foreach ($headers as $header)
            {
                // Collect the headers
                $links = [];

                // If the header has links to process
                if ($header->links->count() > 0)
                {
                    // Loop through all of the links
                    foreach ($header->links->sortBy("order") as $link)
                    {
                        // Determine if the link is currently active (being visited)
                        $link->is_active = $this->linkIsActive($link, $currentRouteName);

                        // If the link has sublinks
                        if ($link->sublinks->count() > 0)
                        {
                            // Add a flag to the link (for expanding/collapsing)
                            $link->expanded = true;
                        } 

                        // Add the link to the collection of this header's links
                        $links[] = $link;
                    }
                }

                // Override the links on the header
                $header->links = $links;

                // Add the header data to the output data
                $out[] = $header;
            }
        }
            

        // Return the output data as a Collection object
        return collect($out);
    }

    /**
     * Link is active
     * Determines if the given link is currently active based on the current route
     * 
     * @param           Hermes\Admin\Models\AdminNavigationLink             The link we're checking
     * @param           string                                              The name of the currently active route
     * @return          boolean
     */
    private function linkIsActive(AdminNavigationLink $link, string $currentRouteName = null)
    {
        if ($link->sublinks->count() > 0)
        {
            if ($currentRouteName == $link->route)
            {
                return true;
            }
            else
            {
                foreach ($link->sublinks as $sublink)
                {
                    if ($currentRouteName == $sublink->route)
                    {
                        return true;
                    }
                }
            }
        }
        else
        {
            if ($currentRouteName == $link->route)
            {
                return true;
            }
        }

        return false;
    }
}